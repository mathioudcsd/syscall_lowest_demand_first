all: 	t1 t2

t1: 	t1.o
	gcc t1.o -o t1

t2: 	t2.o
	gcc t2.o -o t2
	
t1.o: 	demo.c
	gcc -c demo.c -o t1.o
	
t2.o:	demo2.c
	gcc -c demo2.c -o t2.o
	
clean:
	rm t1 t2 t2.o t1.o
