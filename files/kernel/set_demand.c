#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/d_params.h>
#include <asm-generic/errno-base.h>
#include <asm/current.h>

extern int total_demand;

struct task_struct *search_process(int target_pid){
	//int found_flag = 0;
	struct task_struct *p;
	struct task_struct *target=NULL;
	for_each_process(p){
		if(p->pid == target_pid){
			target=p;
			return target;
		}
		else 
			continue;
	}
	return NULL;
}

asmlinkage long sys_set_demand(int pid, int demand){
	printk("mathioud_AM2720 - sys_SET_demand\n");
	struct list_head *p;
	struct task_struct *task;
	struct task_struct *victim;
	struct task_struct *buff;

	struct task_struct *curr_task = get_current();
	int curr_pid = curr_task->pid;

	if(pid<-1){
		printk("Invalid pid(set_demand)\n");
		return EINVAL;

	}

	if(pid==curr_pid || pid==-1){
		if(demand+total_demand<=100){
			curr_task->demand = demand;
			total_demand+=demand;
			printk("PARENT: TOTAL DEMAND = %d\n",total_demand);
			printk("PARENT: demand = %d\n", curr_task->demand);
		}
		else{
			printk("cpu demand time exceeds 100.\n");
			printk("Final total_demand=%d\n",total_demand);
			//total_demand=0;
			return EINVAL;
		}
	} 
	else{
		//printk("MPHKA big else \n");
		victim = search_process(pid);
		// buff = victim;
		if(victim==NULL){
		 	//printk("victim = NULL\n");
			//total_demand=0;
			return EINVAL;
		}
		else{
			if(victim->parent->pid == curr_pid){
				//printk(" VRHKA TO PAIDI\n");
				if(demand+total_demand<=100){
					victim->demand=demand;
					total_demand+=demand;
					printk("CHILD: TOTAL DEMAND = %d\n", total_demand);
					printk("CHILD: demand =%d\n", victim->demand);
					return 0;	
				}
				else{
					printk("The DEMAND for the child, EXCEEDS accepted range, try smaller demand\n");
					printk("cpu demand time exceeds 100.\n",total_demand);
					//total_demand=0;
					return EINVAL;
				}
			}
			else{
				printk("No child with pid=%d found!\n", pid);
				//total_demand=0;
				return EINVAL;
			}
		}
	}		
	//total_demand=0;

}
