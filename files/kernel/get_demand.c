#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/d_params.h>
#include <asm-generic/errno-base.h>
#include <linux/slab.h>


//extern int total_demand;

struct task_struct *s_process(int target_pid){
	//int found_flag = 0;
	struct task_struct *p;
	struct task_struct *target=NULL;
	for_each_process(p){
		if(p->pid == target_pid){
			target=p;
			return target;
		}
		else 
			continue;
	}
	return NULL;
}


asmlinkage long sys_get_demand(int pid, struct d_params *d_arguments){
	struct d_params *bridge;
	bridge = kmalloc(sizeof(struct d_params), GFP_KERNEL);

	struct task_struct *victim;
	int uncopiedbytes;

	struct task_struct *curr_task = get_current();
	int curr_pid = curr_task->pid;

	printk("mathioud_AM2720 - sys_GET_demand\n");

	if(pid<-1){
		printk("Invalid pid(get_demand)\n");
		return EINVAL;
	}
	if(pid==curr_pid || pid==-1){
		victim=s_process(curr_pid);
		//printk("EAUTOULIS pid = %d, currpid=%d\n",victim->pid, curr_pid);

		if(access_ok(VERIFY_WRITE, d_arguments, sizeof(struct d_params))){
			printk("access from user IS ok\n");
			uncopiedbytes = copy_from_user(bridge,d_arguments, sizeof(struct d_params));
			if(uncopiedbytes==0){
				printk("COPY_FROM - SUCCESS\n");
				bridge->demand=victim->demand;
			}
			else
				printk("FROM_USER didn't copy: %d bytes\n",uncopiedbytes);
		}
		else{
			printk("access from user Not ok\n");
			return EINVAL;
		}
		
		if(access_ok(VERIFY_WRITE, d_arguments, sizeof(struct d_params))){
			printk("access to user IS ok\n");
			uncopiedbytes= copy_to_user(d_arguments,bridge, sizeof(struct d_params));
			if(uncopiedbytes==0)
				printk("COPY_TO - SUCCESS\n");
			else
				printk("TO_USER didn't copy: %d bytes\n",uncopiedbytes);
		}
		else{
			printk("access to user Not ok\n");
			return EINVAL;
		}

	}
	else{ //PAIDI
		// 2 problem : to -1 den uparxei ston katalogo
		victim=s_process(pid);
		if(victim==NULL){
		 	printk("victim = NULL\n");
			return EINVAL;
		}
		else{
			if(victim->parent->pid == curr_pid){
				if(access_ok(VERIFY_WRITE, d_arguments, sizeof(struct d_params))){
					printk("access from user IS ok\n");
					uncopiedbytes = copy_from_user(bridge,d_arguments, sizeof(struct d_params));
					if(uncopiedbytes==0){
						printk("COPY_FROM - SUCCESS\n");
						bridge->demand=victim->demand;
					}
					else
						printk("FROM_USER didn't copy: %d bytes\n",uncopiedbytes);
				}
				else{
					printk("access from user Not ok\n");
					return EINVAL;
				}
				
				if(access_ok(VERIFY_WRITE, d_arguments, sizeof(struct d_params))){
					printk("access to user IS ok\n");
					uncopiedbytes= copy_to_user(d_arguments,bridge, sizeof(struct d_params));
					if(uncopiedbytes==0)
						printk("COPY_TO - SUCCESS\n");
					else
						printk("TO_USER didn't copy: %d bytes\n",uncopiedbytes);
				}
				else{
					printk("access to user Not ok\n");
					return EINVAL;
				}
			}
			else{
				printk("No child with pid=%d found!\n", pid);
				return EINVAL;
			}
		}
	}


	// 1 problem : na ginoun ta copy 
	/*
	if(access_ok(VERIFY_WRITE, d_arguments, sizeof(struct d_params))){
		printk("access from user IS ok\n");
		uncopiedbytes = copy_from_user(bridge,d_arguments, sizeof(struct d_params));
		if(uncopiedbytes==0){
			printk("COPY_FROM - SUCCESS\n");
			bridge->demand=victim->demand;
		}
		else
			printk("FROM_USER didn't copy: %d bytes\n",uncopiedbytes);
	}
	else{
		printk("access from user Not ok\n");
		return EINVAL;
	}
	
	if(access_ok(VERIFY_WRITE, d_arguments, sizeof(struct d_params))){
		printk("access to user IS ok\n");
		uncopiedbytes= copy_to_user(d_arguments,bridge, sizeof(struct d_params));
		if(uncopiedbytes==0)
			printk("COPY_TO - SUCCESS\n");
		else
			printk("TO_USER didn't copy: %d bytes\n",uncopiedbytes);
	}
	else{
		printk("access to user Not ok\n");
		return EINVAL;
	} */
	return 0;
}